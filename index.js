var map = new maplibregl.Map({
	container: "map",
	center: [2.2, 41.4],
	zoom: 13,
	style: './style.json'
});

map.addControl(new maplibregl.NavigationControl());

map.on("load", function () {
		
	map.addLayer(
		{
            "id": "escoles_infantil",
            "type": "line",
            "source": "calles",
            "source-layer": "centralities_full",
            "paint": {
                "line-color": [
                    "step",
                    ["get", "p_004C_500"],
                    "rgb(255,87,34)",
                    0.9227489999999999,
                    "rgb(255,152,0)",
                    1.8454979999999999,
                    "rgb(255,193,7)",
                    2.7682469999999997,
                    "rgb(255,235,59)",
                    3.6909959999999997,
                    "rgb(205,220,57)",
                    4.613745,
                    "rgb(139,195,74)",
                    5.536493999999999,
                    "rgb(76,175,80)"
                ],
                "line-width": 2
            },
            "layout": {
                "visibility": "none"
            }
        }
	);

	let targets = {
		"calles_all": "Calles",
		"labels_all": "Street names",
		"grup_10_grup_basic__n_0": "grup bàsic",
		"escoles_infantil": "Escoles Infantil"
	};

    let legendControl = new MaplibreLegendControl(targets, {
		showDefault: true, 
		//showCheckbox: true, 
		onlyRendered: false,
		//reverseOrder: false,
        title: "Layer Switcher"
	});
	map.addControl(legendControl, 'top-right');

    setTimeout(() => {
        legendControl.updateLegendControl();
    }, "1000")
});